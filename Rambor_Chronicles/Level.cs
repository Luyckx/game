﻿using SdlDotNet.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rambor_Chronicles
{
    class Level
    {
        protected int[,] map;
        protected int blocksize;
        protected int index;
        public List<BlockWall> walls;

        public Level(Surface video)
        {

        }

        public void CreateMap(Surface video)
        {
            walls = new List<BlockWall>();
            blocksize = 25;
            index = 0;            
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] == 1)
                    {
                        walls.Add(new BlockWall(new Point(j * 25, i * 25), 25, 25));
                    }
                }
            }
        }

        public void Draw(Surface video)
        {
            foreach (BlockWall wall in walls)
            {
                wall.Draw(video, wall.position);
            }
        }    
          
    }
}
