﻿using SdlDotNet.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rambor_Chronicles
{
    class BlockWall
    {
        private Surface image;
        public Point position;
        public int width;
        public int height;
        public Rectangle collisionRect;

        public BlockWall(Point position, int width, int height)
        {
            this.position = position;
            this.width = width;
            this.height = height;
            image = new Surface(width, height);
            collisionRect = new Rectangle(position.X, position.Y, width, height);
        }

        public void Draw(Surface video, Point position)
        {
            video.Blit(image, position);
        }
       
    }
}
