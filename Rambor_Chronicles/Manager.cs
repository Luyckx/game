﻿using SdlDotNet.Core;
using SdlDotNet.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rambor_Chronicles
{
    class Manager
    {
        private Surface map;
        private Surface mapView;
        private Player player;
        public int screenWidth, screenHeight;
        private Level level1;
        private int offsetX, offsetY;

        public Manager()
        {
            screenWidth = 1300;
            screenHeight = 800;
            offsetX = 0;
            offsetY = 0;
            mapView = Video.SetVideoMode(screenWidth, screenHeight);
            map = new Surface(2000, 1500);

            player = new Player();
            level1 = new Level1(map);

            //Events.Fps = 40;
            Events.Tick += Events_Tick;
            Events.Run();
        }

        private void Events_Tick(object sender, TickEventArgs e)
        {
            map.Fill(Color.White);
            level1.Draw(map);
            
            player.Update(level1);
            PanVisibleScreen();
            player.Draw(map);
            mapView.Blit(map, new Point(0, 0), new Rectangle(offsetX, offsetY, screenWidth, screenHeight));

            //map.Update();
            mapView.Update();
        }

        private void PanVisibleScreen()
        {            
            //BUG: als player naar boven wandelt en collide dan blijft verder pannen terwijl player niet beweegt. Solution: check player position en dan screen terug pannen als position niet is veranderd of iets
            if (player.left == true && offsetX > 0 && player.position.X < 1300) 
                offsetX -= player.speed;
            if (player.up == true && offsetY > 0 && player.position.Y < 1100)
                offsetY -= player.speed;
            if (player.right == true && offsetX < 700 && player.position.X > 700) // offset --> 700 = 2000 - 1300
                offsetX += player.speed;
            if (player.down == true && offsetY < 700 && player.position.Y > 400)
                offsetY += player.speed;
        }
    }
}
