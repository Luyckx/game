﻿using SdlDotNet.Core;
using SdlDotNet.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rambor_Chronicles
{
    class Player
    {
        private Surface playerImage;
        public Point position;
        public bool up, down, left, right;
        public int speed;
        SdlDotNet.Input.Key keyUp, keyDown, keyLeft, keyRight;
        int playerHeight, playerWidth;
        public Rectangle collisionRectangle;

        public Player()
        {
            playerHeight = playerWidth = 50;
            playerImage = new Surface(playerWidth, playerHeight);
            playerImage.ReplaceColor(Color.Black, Color.Blue);   
            speed = 5;
            position = new Point(625, 375); //in midden van scherm
            collisionRectangle = new Rectangle(position.X, position.Y, playerWidth, playerHeight);
            keyUp = SdlDotNet.Input.Key.W;
            keyDown = SdlDotNet.Input.Key.S;
            keyLeft = SdlDotNet.Input.Key.A;
            keyRight = SdlDotNet.Input.Key.D;
            Events.KeyboardDown += Events_KeyboardDown;
            Events.KeyboardUp += Events_KeyboardUp;
        }

        private void Events_KeyboardDown(object sender, SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (e.Key == keyUp)
                up = true;
            if (e.Key == keyDown)
                down = true;
            if (e.Key == keyLeft)
                left = true;
            if (e.Key == keyRight)
                right = true;
        }

        private void Events_KeyboardUp(object sender, SdlDotNet.Input.KeyboardEventArgs e)
        {
            if (e.Key == keyUp)
                up = false;
            if (e.Key == keyDown)
                down = false;
            if (e.Key == keyLeft)
                left = false;
            if (e.Key == keyRight)
                right = false;
        }        

        public void Draw(Surface video)
        {
            video.Blit(playerImage, position);
        }

        public void Update(Level level)
        {           
            screenBorderDetectionPlayer(2000, 1500);

            if (up)
            {                              
                position.Y -= speed;
                collisionRectangle.Y -= speed;

                if (Collides(level)) { 
                    position.Y += speed;
                    collisionRectangle.Y += speed;
                }
            }

            if (down)
            {
                position.Y += speed;
                collisionRectangle.Y += speed;

                if (Collides(level)) {
                    position.Y -= speed;
                    collisionRectangle.Y -= speed;
                }
            }

            if (left)
            {
                position.X -= speed;
                collisionRectangle.X -= speed;

                if (Collides(level)) {
                    position.X += speed;
                    collisionRectangle.X += speed;
                }
            }

            if (right)
            {
                position.X += speed;
                collisionRectangle.X += speed;

                if (Collides(level)) {
                    position.X -= speed;
                    collisionRectangle.X -= speed;
                }
            }
        }

        public bool Collides(Level level)
        {
            foreach (BlockWall wall in level.walls)
            {
                if (collisionRectangle.IntersectsWith(wall.collisionRect))
                {
                    return true;
                }
            }
            return false;
        }

        private void screenBorderDetectionPlayer(int screenWidth, int screenHeight)
        {
            if (position.X <= 0)
                left = false;
            if (position.X >= screenWidth - playerWidth)
                right = false;
            if (position.Y <= 0)
                up = false;
            if (position.Y >= screenHeight - playerHeight)
                down = false;
        }


       
    }
}
